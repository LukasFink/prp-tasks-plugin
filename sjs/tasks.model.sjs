/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class TasksModel {

    constructor(taskService) {
        this.service = taskService;
        this.taskLists = [];
        this.parsedTaskLists = [];
    }

    async parseTaskLists() {
        this.parsedTaskLists.length = 0;
        this.taskLists = await this.service.fetchTaskLists();
        for (let i = 0; i < this.taskLists.length; i++) {
            let tasks = await this.getParsedTasks(this.taskLists[i].tasks);
            let subTasksDone = this.getUndoneTasks(tasks).length;
            let subTasksTotal = this.taskLists[i].tasks.length;

            this.parsedTaskLists.push(
                {
                    taskListId: this.taskLists[i].id,
                    taskListTitle: this.taskLists[i].title,
                    subTasksDone: subTasksDone,
                    subTasksTotal: subTasksTotal,
                    tasks: tasks
                });
        }
    }


    getUndoneTasks(tasks) {
        let undoneTasks = []
        for (let i = 0; i < tasks.length; i++) {
            if (tasks[i].isMarkedAsDone) {
                undoneTasks.push(tasks[i])
            }
        }
        return undoneTasks;
    }

    async getParsedTasks(taskIds) {

        let tasks = []
        for (let i = 0; i < taskIds.length; i++) {
            let task = await this.service.fetchTask(taskIds[i])
            tasks.push(
                this.parseTask(task.id, task.title, task.done, "#/tasks/detailpage?id=" + task.id, 0, 0, task.description, task.dueDate, task.startDate, task.duration, task.taskListId)
            )
        }
        return tasks
    }

    parseTask(taskId, taskTitle, isMarkedAsDone, taskLink, subTasksDone, subTasksTotal, description, dueDate, startDate, duration, taskListId) {
        return {
            taskId: taskId,
            taskTitle: taskTitle,
            isMarkedAsDone: isMarkedAsDone,
            taskLink: taskLink,
            subTasksDone: subTasksDone,
            subTasksTotal: subTasksTotal,
            description: description,
            dueDate: dueDate,
            startDate: startDate,
            duration: duration,
            taskListId: taskListId
        }
    }

    async getTask(taskId) {
        let task = await this.service.fetchTask(taskId);
        return this.parseTask(task.id, task.title, task.done, "link", 0, 0, task.description, task.dueDate, task.startDate, task.duration, task.taskListId)
    }

    async getTaskLists() {
        await this.parseTaskLists();
        return this.parsedTaskLists;
    }

    async getTaskList(taskListId) {
        let taskLists = await this.getTaskLists();
        let list = []
        for (let i = 0, len = taskLists.length; i < len; i++) {
            if (taskLists[i].taskListId == taskListId) {
                list = taskLists[i];
                break
            }
        }
        return list;
    }
}
