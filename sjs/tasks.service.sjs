/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class TasksService {

    constructor() {
        this.token = "Bearer " + window.localStorage.getItem('access_token');
        this.domain = "http://prp.error-cloud.com:8080";
        this.logger = new Logger("TasksService");
    }

    async addTask(task, taskListId) {
        let ref = this;

        await $.ajax({
            url: this.domain + "/api/0.1/tasks/list/" + taskListId + "/task/create",
            method: "POST",
            headers: {
                "Authorization": this.token
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(task),
            success: function (data, status, xhr) {
                ref.logger.log("Task request successful. Data: " + JSON.stringify(data));
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task could not be created. Status: " + status + "; Error: " + error);
            }
        });
    }

    async addTaskList(listTitle) {
        let ref = this;

        await $.ajax({
            url: this.domain + "/api/0.1/tasks/list",
            method: "POST",
            headers: {
                "Authorization": this.token
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: listTitle,
            success: function (data, status, xhr) {
                ref.logger.log("Task list request successful. Data: " + JSON.stringify(data));
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task list could not be created. Status: " + status + "; Error: " + error);
            }
        });
    }

    editTaskList(taskListId, newTitle) {
        let ref = this;

        $.ajax({
            url: this.domain + "/api/0.1/tasks/list/" + taskListId,
            method: "PUT",
            headers: {
                "Authorization": this.token
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: newTitle,
            success: function (data, status, xhr) {
                ref.logger.log("Task list request successful. Data: " + JSON.stringify(data));
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task list could not be edited. Status: " + status + "; Error: " + error);
            }
        });
    }

    async editTask(task) {
        let ref = this;

        await $.ajax({
            url: this.domain + "/api/0.1/tasks/" + task.id,
            method: "PUT",
            headers: {
                "Authorization": this.token
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(task),
            success: function (data, status, xhr) {
                ref.logger.log("Task request successful. Data: " + JSON.stringify(data));

            },
            error: function (xhr, status, error) {
                ref.logger.log("Task could not be edited. Status: " + status + "; Error: " + error);

            }
        });
    }

    async fetchTaskLists() {
        let ref = this;

        try {
            const result = await $.ajax({
                url: this.domain + "/api/0.1/tasks/list",
                method: "GET",
                headers: {
                    "Authorization": this.token
                },
            });

            ref.logger.log("Task list request successful. Data: " + JSON.stringify(result));
            return result;

        } catch (error) {
            ref.logger.log("Task lists could not be fetched. Status: " + status + "; Error: " + error);
            return [];
        }
    }

    async fetchTask(id) {
        let ref = this;

        try {
            const result = await $.ajax({
                url: this.domain + "/api/0.1/tasks/" + id,
                method: "GET",
                headers: {
                    "Authorization": this.token
                }
            });
            ref.logger.log("Task request successful. Data: " + JSON.stringify(result));
            return result;

        } catch (error) {
            ref.logger.log("Task could not be fetched. Status: " + status + "; Error: " + error);
        }
    }

    deleteTaskList(id) {
        let ref = this;

        $.ajax({
            url: this.domain + "/api/0.1/tasks/list/" + id,
            method: "DELETE",
            headers: {
                "Authorization": this.token
            },
            success: function (data, status, xhr) {
                ref.logger.log("Task list delete request successful.");
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task list could not be deleted. Status: " + status + "; Error: " + error);
            }
        });
    }

    deleteTask(id) {
        let ref = this;

        $.ajax({
            url: this.domain + "/api/0.1/tasks/" + id,
            method: "DELETE",
            headers: {
                "Authorization": this.token
            },
            success: function (data, status, xhr) {
                ref.logger.log("Task delete request successful.");
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task could not be deleted. Status: " + status + "; Error: " + error);
            }
        });
    }

    markAsDone(taskId) {
        let ref = this;

        $.ajax({
            url: this.domain + "/api/0.1/tasks/" + taskId + "/done",
            method: "PUT",
            headers: {
                "Authorization": this.token
            },
            success: function (data, status, xhr) {
                ref.logger.log("Task " + taskId + " marked as done. Data: " + JSON.stringify(data));
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task could not be marked as done. Status: " + status + "; Error: " + error);
            }
        });
    }

    markAsUndone(taskId) {
        let ref = this;

        $.ajax({
            url: this.domain + "/api/0.1/tasks/" + taskId + "/undone",
            method: "PUT",
            headers: {
                "Authorization": this.token
            },
            success: function (data, status, xhr) {
                ref.logger.log("Task " + taskId + " marked as undone. Data: " + JSON.stringify(data));
            },
            error: function (xhr, status, error) {
                ref.logger.log("Task could not be marked as undone. Status: " + status + "; Error: " + error);
            }
        });
    }

}
