class TranslationLoader {

    constructor() {
        this.trans = null;
        this.logger = new Logger("TranslationLoader");
    }

    loadTranslation() {
        if (this.trans == null) {
            this.trans = de.DE();
        }

        this.logger.log("Created translation object.");
        return this.trans;
    }

}